'use strict';

var _user = _interopRequireDefault(require("../controllers/user.js"));

var _authenticate = _interopRequireDefault(require("../middelware/authenticate.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');

var api = express.Router();
api.get('/test', _user.default.prueba);
api.post('/Register', _user.default.saveUser);
api.post('/login', _user.default.loginUser);
api.put('/update-user/:id', _authenticate.default.ensureAuth, _user.default.updateUser);
api.get('/users/:page?', _authenticate.default.ensureAuth, _user.default.getUsers);
api.get('/userone/:id', _authenticate.default.ensureAuth, _user.default.getUserOne);
module.exports = api;