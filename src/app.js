'use strict'
import express from 'express';
import path from 'path';
const app = express();

import userRoute from './route/user.js';
import productController from './route/product.js';
import bodyParser from 'body-parser';
//cargar Rutas
// protocol https para no redireccionar a http



app.use(bodyParser.urlencoded({extended:false,limit:'50mb', parameterLimit:50000}));
app.use(bodyParser.json({limit:'50mb'}));

//configurar cabeceras http
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
	res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
	next();
});


app.get('/api',(req,res)=>{
    res.json({message:'Welcome to Api'});
})

//Cargamos una ruta estatica que es la carpeta client
 app.use('/',express.static('dist/client',{redirect:false}));


 app.use('/api',userRoute);

 app.use('/api',productController);


 //En el caso que se cargue cualquier otra ruta que no sea la raiz que cargue la rura 'client/index.html'
 


app.get('*',function (req,res,next) {
res.sendFile(path.resolve('dist/client/index.html'));

 });
module.exports=app;