'use strict';

var _product = _interopRequireDefault(require("../controllers/product.js"));

var _authenticate = _interopRequireDefault(require("../middelware/authenticate.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');

var api = express.Router();
api.post('/product', _product.default.createProduct);
api.put('/product', _product.default.UpdateProduct);
api.get('/product/:slug', _product.default.getProductOne);
api.delete('/product/:id', _product.default.DeleteProduct);
api.post('/product-image/:id', _product.default.uploadImage);
api.get('/product-image/:image', _product.default.getImageFile);
api.get('/product', _product.default.getProduct);
module.exports = api;