'use strict';

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _user = _interopRequireDefault(require("./route/user.js"));

var _product = _interopRequireDefault(require("./route/product.js"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express.default)();
//cargar Rutas
// protocol https para no redireccionar a http
app.use(_bodyParser.default.urlencoded({
  extended: false,
  limit: '50mb',
  parameterLimit: 50000
}));
app.use(_bodyParser.default.json({
  limit: '50mb'
})); //configurar cabeceras http

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});
app.get('/api', (req, res) => {
  res.json({
    message: 'Welcome to Api'
  });
}); //Cargamos una ruta estatica que es la carpeta client

app.use('/', _express.default.static('dist/client', {
  redirect: false
}));
app.use('/api', _user.default);
app.use('/api', _product.default); //En el caso que se cargue cualquier otra ruta que no sea la raiz que cargue la rura 'client/index.html'

app.get('*', function (req, res, next) {
  res.sendFile(_path.default.resolve('dist/client/index.html'));
});
module.exports = app;