'use strict';

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || 9000;

var app = require('./app');

_mongoose.default.connect('mongodb://shop:wsxPptyPNE37#T2@ds141128.mlab.com:41128/shop', {
  useNewUrlParser: true
}, (err, res) => {
  if (err) {
    throw err;
  } else {
    console.log('La base de datos esta corriendo correctamente');
    app.listen(port, function () {
      console.log('Aplicacion corriendo en el puerto ' + port);
    });
  }
});