'use strict'

var fs = require('fs');
var path=require('path');
var bcrypt=require('bcrypt-nodejs');
//var mongoosePaginate=require('mongoose-paginate');

var User= require('../models/user');

var jwt =require('../service/jwt');

function prueba(req,res) {
    res.status(200).send({message:'Probando Controlador de Usuarios'});
}

function saveUser(req,res){
    var user= new User();

    // var params= req.boby;

    console.log(req.body);
    user.name=req.body.name;
    user.surname=req.body.surname;
    user.email=req.body.email;
    user.image='null';
    user.activo='1';
  

    var u=User.find({email:user.email}).sort();

    u.exec(function (err,users) {
        if (err) {
           return res.status(505).send({message:'Error en el servidor '+err});
        }  else {
                
                if(users.length>=1){
                    for(var i=0;i<users.length;i++){
                        if(req.body.email==users[i].email){
                             res.status(200).send({message:'El E-mail ya esta registrado'});
                        }
                    }
                }else{
                     //res.status(200).send({message:'estas registrado'});
                      if (req.body.password) {
                        //Enciptar contrasena y datos
                        bcrypt.hash(req.body.password,null,null,function (err,hash) {
                        user.password=hash;
                        if (user.name!=null && user.surname!=null && user.email!=null) {
                            user.save((err,userStored)=>{
                                    if (err) {
                                        res.status(500).send({message:'Error al guardar los datos',Description:err});
                                    } else {
                                        if (!userStored) {
                                           return res.status(400).send({message:'No se ha registrado el usuario'});
                                        } else {
                                          
                                        return res.status(200).send({user:userStored});
                                           
                                        }
                                    }
                            });
                        } else {
                           return res.status(200).send({message:'Rellena todos los campos'});
                        } 
                        });
                    } else {
                       return res.status(500).send({message:'Introduzca la contraseña'});
                    }
                } 
            }
        
    });

    
}

function loginUser(req,res) {
    var email=req.body.email;
    var password=req.body.password;

    var q=User.findOne({email:email.toLowerCase()});
    var search=User.findOne({email:email.toLowerCase()});

    search.exec(function (err,u) {
        if (err) {
            res.status(500).send({message:'Error en la peticion'});
        } else {
            if (!u) {
                res.status(500).send({message:'El usuario no existe'});
            } else {
                if (u.activo=='0') {
                    return res.status(200).send({message:'Esta cuenta aun no esta activa'});
                } else {
                    q.populate({path:'roles'}).exec(function (err,user) {
                        if (err) {
                            res.status(500).send({message:'Error en la peticion'});
                            
                        } else {
                            if (!user) {
                                res.status(200).send({message:'El usuario no existe en la base de datos'});
                                
                            } else {
                                    bcrypt.compare(password,user.password,function (arr,check) {
                                if (check) {
                                    //Comprobar datos
                                    if (req.body.gethash) {
                                        res.status(200).send({token:jwt.createToken(user)});
                                    } else {
                                        res.status(200).send({user});
                                    }
                                } else {
                                    res.status(200).send({message:'El usuario no ha podido logearse'});
                                } 
                                });
                            }
                
                           
                        }
                    });
                }
            }
        } 
    });

    
}


function updateUser(req,res){
    var UserId=req.params.id;
    var update=req.body;

    User.findByIdAndUpdate(UserId,update,(err,userUpdate)=>{
        if (err) {
            res.status(505).send({message:'Error en la Peticion'});
        } else {
            if (!userUpdate) {
                res.status(404).send({message:'No se actualizo el Ususario'});                
            } else {
                res.status(200).send({user:userUpdate});
            }
        }
    });
}

function getUserOne(req,res) {
    var UserId=req.params.id;

    User.findById(UserId,(err,userone)=>{
        if (err) {
            res.status(500).send({message:'Error en el server'});
        } else {
            if (!userone) {
                res.status(404).send({message:'Error Interno'});
            } else {
                res.status(200).send({userone:userone});
            }
        }
    });
}


function getUsers(req,res) {
    if (req.params.page) {
        var page=req.params.page;
    } else {
        var page=1;
    }

    var itemPerPage=3;

    User.find().sort('name').paginate(page,itemPerPage,function(err,users,total) {
       if (err) {
        res.status(500).send({message:'Error en la peticion'});        
       } else {
           if (!users) {
            res.status(404).send({message:'No hay Usuarios'});
            
           } else {
            res.status(200).send({
                total_items:total,
                users:users
            });
            
               
           }
       } 
    });

}

module.exports={
    prueba,
    saveUser,
    loginUser,
    updateUser,
    getUsers,
    getUserOne
};