'use strict'

var mongoose=require('mongoose');
var slug = require('slug');
var Schema= mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var ProductSchema=new Schema({
    name:String,
    slug:String,
    price:String,
    description:String,
    date:String,
    image:String,
    
    
});
ProductSchema.plugin(mongoosePaginate);
module.exports=mongoose.model('Product',ProductSchema);