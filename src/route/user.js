'use strict'

var express= require('express');
import UserController from '../controllers/user.js';

var api = express.Router();

import md_auth from '../middelware/authenticate.js';

api.get('/test',UserController.prueba);
api.post('/Register',UserController.saveUser);
api.post('/login',UserController.loginUser);
api.put('/update-user/:id',md_auth.ensureAuth,UserController.updateUser);
api.get('/users/:page?',md_auth.ensureAuth,UserController.getUsers);
api.get('/userone/:id',md_auth.ensureAuth,UserController.getUserOne);

module.exports=api;