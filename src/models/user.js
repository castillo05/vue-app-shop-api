'use strict'

var mongoose=require('mongoose');
var Schema= mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var UserSchema=new Schema({
    name:String,
    surname:String,
    email:String,
    password:String,
    image:String,
    activo:String
    
});
UserSchema.plugin(mongoosePaginate);

module.exports=mongoose.model('User',UserSchema);