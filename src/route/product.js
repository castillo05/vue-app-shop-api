'use strict'

var express= require('express');

import ProductController from '../controllers/product.js';

var api = express.Router();

import md_auth from '../middelware/authenticate.js';


api.post('/product',ProductController.createProduct);
api.put('/product',ProductController.UpdateProduct);
api.get('/product/:slug',ProductController.getProductOne);
api.delete('/product/:id',ProductController.DeleteProduct);
api.post('/product-image/:id',ProductController.uploadImage);
api.get('/product-image/:image',ProductController.getImageFile);
api.get('/product',ProductController.getProduct);

module.exports=api;