'use strict'
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var Product = require('../models/product');
var slug = require('slug');

var multer  = require('multer')






// Metodo para Guardar el Post
function createProduct(req, res) {


     // Metodo para Subir imagen del Post

    const storage = multer.diskStorage({
        destination: path.join(__dirname, '../public/uploads'),
        filename:  (req, file, cb) => {
            cb(null, file.originalname);
        }
    })
    const uploadImage2 = multer({
        storage,
        limits: {fileSize: 1000000}
    }).single('image');
   
    
    
    
    uploadImage2(req, res, (err) => {
                        if (err) {
                            err.message = 'The file is so heavy for my service';
                            return res.send(err);
                        }
                         var product = new Product();
                         var slugName=req.body.name+Math.random();
                        product.name = req.body.name;
                        product.slug = slug(slugName);
                        product.description = req.body.description;
                        product.price = req.body.price;
                        product.date = moment().format();
                        product.image=req.file.originalname;
                         product.save((err, postStored) => {
                            if (err) {
                                res.status(500).send({ err });
                            } else {
                                if (!postStored) {
                                    res.status(404).send({ message: 'No se guardo el Post' });
                                } else {
                                    res.status(200).send({ post: postStored });
                                  
                                }
                            }
                        });

                        console.log(req.file);
                        
    });
   

   
}


// Metodo para Actualizar el Post

function UpdateProduct(req, res) {
    var productId = req.params.id;
    var update = req.body;

    Product.findByIdAndUpdate(productId, update, (err, productupdate) => {
        if (err) {
            res.status(500).send({ message: 'Error en la peticion' });
        } else {
            if (!productupdate) {
                res.status(404).send({ message: 'No se pudo actualiza el Post' });
            } else {
                res.status(200).send({ product: productupdate });
            }
        }
    });
}

// Metodo para eliminar el post

function DeleteProduct(req, res) {
    var ProductId = req.params.id;

    Product.findByIdAndRemove(ProductId, (err, productremove) => {
        if (err) {
            res.status(500).send({ message: 'Error en la peticion' });
        } else {
            if (!productremove) {
                res.status(404).send({ message: 'No se elimino el post' });
            } else {
                res.status(200).send({ product: productremove });
            }
        }
    });
}

// Metodo para mostrar el post
function getProduct(req, res) {
    let datePerPage=1;
    if(req.params.page){
        datePerPage=req.params.page;
    }

    const option={
        
        sort:     { date: -1 },
        populate: 'author',
        lean:     true,
        page:datePerPage,
        limit:    200
    }
    Product.paginate({},option).then((result)=>{
        if(!res) return res.status(500).send({message:'Error interno del servidor'});
        res.status(200).send({product:result});
    }).catch(error=>{
        console.log(error);
    });
  
     
}


// Metodo para mostrar el ultimo post

function EndProduct(req, res) {
    var query = Product.find().sort({ _id: -1 }).limit(1);

    query.exec(function(err, product) {
        if (err) {
            res.status(500).send({ message: 'Error en el server' });
        } else {
            if (!product) {
                res.status(404).send({ message: 'No hay Post' });
            } else {
                res.status(200).send({ product: product });
            }
        }
    });
}


// Metodo para Mostrar detalles del Product
function getProductOne(req, res) {
    var slug = req.params.slug;
    console.log(slug)
  
    Product.findOne({ slug: slug }, function(err, product) {
        if (err) {
            res.status(500).send({ message: 'Error en el server' });
        } else {
            if (!product) {
                res.status(404).send({ notfound: 'Esta publicacion no existe corrija la URL' });
            } else {
                res.status(200).send({ 
                    product: product,
                    status:'200'
                });
            }
        }


    });

}



let uploadImage=(req, res)=> {
   try {

    uploadImage2(req, res, (err) => {
        if (err) {
            err.message = 'The file is so heavy for my service';
            return res.send(err);
        }
        console.log(req.file.filename);
        res.send('uploaded');
    });
  


   } catch (error) {
       console.log(error);
   }
   
    
}



// Metodo para mostrar Imagen del Post

function getImageFile(req, res) {
    // body...
   
    var imageFile = req.params.image;
    var path_file =  path.join(__dirname, '../public/uploads/');
    
    fs.exists(path_file + imageFile, function(exists) {
        console.log(exists)
        if (exists) {
            res.sendFile(path.resolve(path_file + imageFile));

        } else {
            res.status(200).send({ message: 'No existe la imagen' });

        }
        // body...
    });
}





// Exportamos como Modulos cada metodo
module.exports = {
    createProduct,
    UpdateProduct,
    DeleteProduct,
    getProduct,
    EndProduct,
    getProductOne,
    uploadImage,
    getImageFile





};